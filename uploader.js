(function ( $ ) {

    $.fn.prettyUp = function(options) {
        
        var settings = $.extend({
                url: "./"
            ,   removeOnUpload: true
            ,	fadeOutTime: 1000
        }, options );

        var $that = this;

        $(this).on('change', function(e) {
            var files = e.target.files;
            for (var i = 0, f; f = files[i]; i++) {
            	uploadfile(f);
            }
            $that.replaceWith( $that = $that.clone( true ) );
        });

        var tests = {
                filereader: typeof FileReader != 'undefined'
            ,   dnd: 'draggable' in document.createElement('span')
            ,   formdata: !!window.FormData
          	,   progress: "upload" in new XMLHttpRequest
        }

        var uploadfile = function (f) {
        	var $el;
        	var formData = tests.formdata ? new FormData() : null;

            if (f.type.match('image.*') && tests.filereader) {
                
                var reader = new FileReader();
                
                // Closure to capture the file information.
        		reader.onload = (function(theFile) {
        			return function(e) {
        				// Render thumbnail.
        				var span = document.createElement('span');
        				pBar = '<div class="resume"><div id="docs-progress-bar" class="progress-indicator"><span class="progress-indicator-value"></span></div></div>';
        				span.innerHTML = ['<img width="150px" class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"/>', pBar].join('');

        				$('.file-preview').append(span);
        				$el = $(span).find('#docs-progress-bar');
        			};
        		})(f);
                
                reader.readAsDataURL(f);
            	formData.append(0, f);
            
        	    if (tests.formdata) {
        	            xhr = new XMLHttpRequest();
        	            xhr.open('POST', settings.url);
        	            xhr.onload = function() {
        	            	progressBar.update($el, 1);
        	            	if(settings.removeOnUpload) $el.parents('span').fadeOut(settings.fadeOutTime,function() { $el.remove() });
        	            };

        	        if (tests.progress) {
        	            xhr.upload.onprogress = function (event) {
        	                if (event.lengthComputable) {
        	                    percent = (event.loaded / event.total | 0) * 100;
        	                    progressBar.update($el, percent / 100);
        	                }
        	            }
        	        }
        	        xhr.send(formData);
        	    }

          	}
        }

        var progressBar = {

        	reset: function (element) {
            	var $progressBarContainer = $(element).first();
                var $progressBar = $progressBarContainer.children(".aui-progress-indicator-value");

                $progressBarContainer.removeAttr("data-value");
                $progressBar.css("width", "100%");
            },

            update: function (element, value) {
                var $progressBarContainer = $(element).first();
                var $progressBar = $progressBarContainer.children(".progress-indicator-value");
                var currentProgress = $progressBar.attr("data-value") || 0;

                var afterTransitionEvent = "progress-indicator-after-update";
                var beforeTransitionEvent = "progress-indicator-before-update";
                var transitionEnd = "transitionend webkitTransitionEnd";

                var isIndeterminate = !$progressBarContainer.attr("data-value");

                //if the progress bar is indeterminate switch it.
                if(isIndeterminate){
                    $progressBar.detach().css("width", 0).appendTo($progressBarContainer);
                }

                if(typeof value === "number" && value<= 1 && value >= 0){
                    //detect whether transitions are supported
        			window.setTimeout(function(){
                        $progressBar.css("width", value * 100 + "%")
                        $progressBarContainer.attr("data-value", value);
                    }, 0);
                }
                return $progressBarContainer;
            }
        }
    }

}( jQuery ));